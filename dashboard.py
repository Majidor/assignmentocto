import time
import streamlit as st
import requests
import pandas as pd
from PIL import Image

from src.constants import API_URL, DATASET_PATH, INFERENCE_EXAMPLE, CM_PLOT_PATH
from src.training.train_pipeline import TrainingPipeline

from src.ui.plots import Plots
from src.ui.data import DataInfo


parts = {
    "EDA": {
        "title": "Exploratory Data Analysis",
        "info": "In this section, you are invited to create insightful graphs about the card fraud dataset that you were provided.",
    },
    "Training": {
        "title": "Training",
        "info": "Before you proceed to training your model. Make sure you have checked your training pipeline code and that it is set properly.",
    },
    "Inference": {
        "title": "Fraud Inference",
        "info": "This section simplifies the inference process. You can tweak the values of feature 1, 2, 19, and the transaction amount and observe how your model reacts to these changes",
    },
}



st.title("Card Fraud Detection Dashboard")
st.sidebar.title("Data Themes")

sidebar_options = st.sidebar.selectbox("Options", parts.keys())
st.header(parts[sidebar_options]["title"])
st.sidebar.info(parts[sidebar_options]["info"])

if sidebar_options == "EDA":
    dataI = DataInfo(DATASET_PATH)
    data = dataI.data
    data_sample = dataI.data_sample()
    data_description = dataI.data_describe()
    class_infos = dataI.get_class_infos()

    plots = Plots(data)
    fig_count = plots.get_count_plot()
    corr_plot = plots.get_corr_plot()
    fig_amount = plots.get_amount_plot()


    """
    #### Dataframe Sample
    """
    st.write(data_sample)

    """
    #### Dataframe Description
    """
    st.write(data_description)
    st.write(f"Amount mean: {data_description['Amount']['mean']}")

    nan_cols = [i for i in data.columns if data[i].isnull().any()]
    st.markdown(f"There are **{len(nan_cols)}** columns with NaN values : **{nan_cols}**")

    st.markdown("---")

    """
    ### Infos
    """
    col1, col2 = st.columns(2)

    with col1:
        st.markdown(
            f"Max Amount : **{data_description['Amount']['max']}**"
        )
        st.markdown(
            f"Min Amount : **{data_description['Amount']['min']}**"
    )

    with col2:
        fraud, clean = class_infos['Fraud'], class_infos['Clean']
        st.markdown(
            f"Fraud range : **{fraud['Min']}** -> **{fraud['Max']}**"
        )
        st.markdown(
            f"Clean range : **{clean['Min']}** -> **{clean['Max']}**",
        )

    st.markdown("---")

    """
    #### Correlation
    """
    # st.write(corr_plot)
    st.dataframe(dataI.data.corr())

    neg, pos = st.columns(2)
    with neg:
        st.markdown("Negative correlation : ")
        st.markdown("- Time / V3")
        st.markdown("- Amount / V2")
        st.markdown("- Amount / V5")

    with pos:
        st.markdown("Positive correlation : ")
        st.markdown("- Amount / V7")
        st.markdown("- Amount / V20")

    st.markdown("---")

    """
    ### Plots
    """

    """
    #### Count Plot
    """
    st.write(fig_count)
    clean_count, fraud_count = st.columns(2)
    clean_count.metric("Clean Transactions", class_infos["Clean"]["Count"])
    fraud_count.metric("Fraud Transactions", class_infos["Fraud"]["Count"])
    st.markdown("This is an **Imbalanced dataset**")

    """
    #### Amount Distribution Plots
    """
    st.write(fig_amount)
    st.markdown(f"Transactions **below** 100 : {len(data[data['Amount'] < 100])}")
    st.markdown(f"Transactions **above** 100 : {len(data[data['Amount'] > 100])}")


elif sidebar_options == "Training":
    name = st.text_input("Model name", placeholder="decisiontree")
    print("File to save = ", name)
    serialize = st.checkbox("Save model")
    train = st.button("Train Model")

    if train:
        with st.spinner("Training model, please wait..."):
            time.sleep(1)
            try:
                tp = TrainingPipeline()
                tp.train(serialize=serialize, model_name=name)
                tp.render_confusion_matrix(plot_name=name)
                accuracy, f1 = tp.get_model_perfomance()
                col1, col2 = st.columns(2)

                col1.metric(label="Accuracy score", value=round(accuracy, 4))
                col2.metric(label="F1 score", value=round(f1, 4))

                st.image(Image.open(CM_PLOT_PATH))

            except Exception as e:
                st.error("Failed to train model!")
                st.exception(e)


else:
    # F1
    feature_1 = st.slider("Transaction Feature 1", -10.0, 10.0, step=0.01, value=-4.075)
    # F2
    feature_2 = st.slider("Transaction Feature 2", -10.0, 10.0, step=0.01, value=0.963)
    # F29
    feature_19 = st.slider(
        "Transaction Feature 19", -10.0, 10.0, step=0.01, value=2.630
    )
    # Amount
    amount = st.number_input(
        "Transaction Amount", value=1000, min_value=0, max_value=int(1e10), step=100
    )

    infer = st.button("Run Fraud Inference")

    if infer:
        INFERENCE_EXAMPLE[1] = feature_1
        INFERENCE_EXAMPLE[2] = feature_2
        INFERENCE_EXAMPLE[19] = feature_19
        INFERENCE_EXAMPLE[28] = amount
        with st.spinner("Running inference..."):
            time.sleep(1)
            try:
                result = requests.post(
                    API_URL + "/inference", json=INFERENCE_EXAMPLE
                )
                st.write(result.text + " : " + type(result.text).__name__)
                result = "Clean" if int(result.text)==0 else "Fraud"
                st.success("Inference Done!")
                predictions = requests.get(API_URL + "/predictions")
                pr_df = pd.read_json(predictions.text)

                st.dataframe(pr_df)
            except Exception as e:
                st.error("Failed to call Inference API!")
                st.exception(e)

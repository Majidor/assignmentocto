from datetime import datetime
import json

import numpy as np
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from src.constants import FINAL_MODEL, SQLALCHEMY_DATABASE_URI
from src.models.aggregator_model import AggregatorModel

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI

db = SQLAlchemy(app)
@app.before_first_request
def create_tables():
    db.create_all()


class Prediction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=datetime.now)
    feat1 = db.Column(db.Float, nullable=False)
    feat2 = db.Column(db.Float, nullable=False)
    feat19 = db.Column(db.Float, nullable=False)
    amount = db.Column(db.Float, nullable=False)
    prediction = db.Column(db.String(10), nullable=False)

    def __init__(self, feat1, feat2, feat19, amount, prediction):
        self.feat1 = feat1
        self.feat2 = feat2
        self.feat19 = feat19
        self.amount = amount
        self.prediction = prediction

    def to_dict(self):
        return {
            "id": self.id,
            "date": self.date.strftime(r"%Y-%m-%d %H:%M:%S"),
            "feature1": self.feat1,
            "feature2": self.feat2,
            "feature19": self.feat19,
            "amount": self.amount,
            "prediction": self.prediction,
        }


model = AggregatorModel()
model.load(FINAL_MODEL)


@app.route("/")
def index():
    return "CARD FRAUD DETECTION API"

@app.route("/predictions", methods=["GET"])
def get_predictions():
    if request.method == "GET":
        return json.dumps([pred.to_dict() for pred in Prediction.query.all()])

@app.route("/inference", methods=["POST"])
def run_inference():
    if request.method == "POST":
        features = np.array(request.json).reshape(1, -1)
        prediction = model.predict(features)
        features = features.tolist()[0]
        pred = Prediction(
            feat1=features[1],
            feat2=features[2],
            feat19=features[19],
            amount=features[28],
            prediction='Clean' if int(prediction[0])==0 else 'Fraud',
        )
        db.session.add(pred)
        db.session.commit()
        return str(prediction[0])

if __name__ == "__main__":
    app.run()

import streamlit as st
import seaborn as sns
import matplotlib.pyplot as plt


class Plots():
    def __init__(self, df):
        self.df = df

    def get_count_plot(self):
        fig_count, ax = plt.subplots(figsize=(10, 10))
        ax = sns.countplot(x="Class", data=self.df)
        for p in ax.patches:
            ax.annotate(p.get_height(), (p.get_x() + 0.25, p.get_height() + 0.01))
        return fig_count


    def get_corr_plot(self):
        corr_plot, _ = plt.subplots(figsize=(10, 10))
        sns.heatmap(
            self.df.corr(),
            cmap="coolwarm",
            annot=True,
            fmt=".2f",
            annot_kws={"fontsize": 6},
            vmin=-1,
            vmax=1,
            square=True,
            linewidths=0.01,
            linecolor="black",
            cbar=False,
        )

        sns.despine(top=True, right=True, left=True, bottom=True)
        return corr_plot


    def get_amount_plot(self):
        fig = plt.figure(figsize=(10, 10))
        ax = sns.violinplot(x = "Class", y = "Amount", data = self.df)
        ax.set_xlabel("Class")
        ax.set_ylabel("Amount")
        return fig

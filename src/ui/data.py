import pandas as pd
import streamlit as st

class DataInfo():
	def __init__(self, DATASET_PATH):
		self.data = pd.read_csv(DATASET_PATH)

	def data_describe(self):
		return self.data.describe()


	def data_sample(self, nrows=5):
		return self.data.sample(nrows)


	def get_class_infos(self):
		counts = self.data.Class.value_counts().values
		return {
			"Clean": {
				"Count": int(counts[0]),
				"Max": self.data[self.data["Class"] == 0].Amount.max(),
				"Min": self.data[self.data["Class"] == 0].Amount.min(),
			},
			"Fraud": {
				"Count": int(counts[1]),
				"Max": self.data[self.data["Class"] == 1].Amount.max(),
				"Min": self.data[self.data["Class"] == 1].Amount.min(),
			},
		}